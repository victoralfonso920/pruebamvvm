package com.victorDev.myapplication.di.module

import com.victorDev.myapplication.network.RetrofitInterface
import com.victorDev.myapplication.di.repository.InyecRepository
import com.victorDev.myapplication.tools.Const
import com.victorDev.myapplication.view.init.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

// Created by Victor Hernandez on 09/07/2020.
// Proyect My Application
//contact victoralfonso92@gmail.com

object InyectModule {

  val inyectModule:Module = module {
      single { InyecRepository().getRetrofit<RetrofitInterface>(Const.URL)}
      single { InyecRepository().provideGson()}
      viewModel { MainViewModel(get()) }
  }


}