package com.victorDev.myapplication.network

import com.victorDev.myapplication.model.categories.Categories
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

// Created by Victor Hernandez on 09/07/2020.
// Proyect My Application
//contact victoralfonso92@gmail.com

interface RetrofitInterface {

    //obtener array
    @GET("{sufijourl}")
    suspend fun getDatos(
        @Path("sufijourl") sufijourl: String,
        @QueryMap options: Map<String, String>
    ): Response<List<Categories>>

}