package com.victorDev.myapplication.model.categories

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

// Created by Victor Hernandez on 09/07/2020.
// Proyect My Application
//contact victoralfonso92@gmail.com
@Parcelize
data class Categories(
    val virtuemart_category_id: String? = "",
    val category_name: String? = "",
    val estado: String? = "",
    val logoNuevo: String? = ""
):Parcelable