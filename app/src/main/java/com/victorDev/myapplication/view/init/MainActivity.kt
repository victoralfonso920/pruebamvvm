package com.victorDev.myapplication.view.init

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.victorDev.myapplication.R
import com.victorDev.myapplication.model.categories.Categories
import com.victorDev.myapplication.tools.DynamicAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_recycler_home.view.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {


   val viewModel by inject<MainViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        observerData()
    }

    fun observerData(){
        viewModel.getCategories().observe(this, Observer {
            it?.let {
                    updateListOptions(it)
            }
        })
    }

    private fun updateListOptions(cat:List<Categories>) {
       val optionsAdapter = getAdapterOptions(cat)
        optionsAdapter?.let { dy ->
            rcCategorias?.configureRecycler(dy)
            rcCategorias.setHasFixedSize(false)
            rcCategorias.isNestedScrollingEnabled = false
            dy.notifyDataSetChanged()
        }
    }

    fun getAdapterOptions(cat:List<Categories>): DynamicAdapter<Categories> {
        var adapter: DynamicAdapter<Categories>? = null
        try {
            adapter = DynamicAdapter(
                R.layout.item_recycler_home,
                cat,
                fun(vh, view, dt, p) {
                   view.imgRecy fondoURLBinding dt.logoNuevo!!
                    view.textRecy.text = dt.category_name
                })
        } catch (e: Exception) {
            println(e.message)
        }
        return adapter!!
    }

}


fun <T> RecyclerView.configureRecycler(
    adapter: DynamicAdapter<T>,
    isVertical: Boolean = true,
    isgrid: Boolean = false,
    numColumns: Int = 2
) {
    itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
    if (isgrid) {
        layoutManager = androidx.recyclerview.widget.GridLayoutManager(context, numColumns)
    } else {
        layoutManager = LinearLayoutManager(
            context,
            if (isVertical) RecyclerView.VERTICAL else RecyclerView.HORIZONTAL, false
        )
    }

    this.adapter = adapter
}


infix fun ImageView.fondoURLBinding(url: String) {
    try {
        val options = RequestOptions()
            .priority(Priority.NORMAL)
            .format(DecodeFormat.PREFER_RGB_565)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .fitCenter()

        Glide.with(this)
            .asBitmap()
            .load(url)
            .apply(options)
            .into(this)
    } catch (e: Exception) {
    }

}