package com.victorDev.myapplication.view.init

import androidx.lifecycle.ViewModel

import androidx.lifecycle.liveData

import com.victorDev.myapplication.network.RetrofitInterface
import com.victorDev.myapplication.tools.Const

import java.util.HashMap

// Created by Victor Hernandez on 09/07/2020.
// Proyect My Application
//contact victoralfonso92@gmail.com

class MainViewModel(private val api: RetrofitInterface) : ViewModel(){


   fun getCategories() = liveData {
       val data = HashMap<String, String>()
       data["seccion"] = "gex_catalogo_categorias"
       data["version_codigo"] = "1"
       val result = api.getDatos(Const.SUFIJO_URL,data).body()
       emit(result)
   }


}

