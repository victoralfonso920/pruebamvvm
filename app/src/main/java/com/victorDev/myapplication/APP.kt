package com.victorDev.myapplication

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.victorDev.myapplication.di.module.InyectModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

// Created by Victor Hernandez on 09/07/2020.
// Proyect My Application
//contact victoralfonso92@gmail.com

class APP:Application() {

    companion object{
        @SuppressLint("StaticFieldLeak")
        lateinit var instance: APP private set
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = instance.applicationContext
        startKoin {
            androidLogger()
            androidContext(this@APP)
            // load modules
            modules(InyectModule.inyectModule)
        }

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}